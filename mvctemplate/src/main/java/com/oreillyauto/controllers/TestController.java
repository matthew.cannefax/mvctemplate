package com.oreillyauto.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.oreillyauto.domain.Test;
import com.oreillyauto.service.TestService;

@Controller
public class TestController {

	@Autowired
	TestService testService;
	
	@GetMapping("/test")
	public String getTest(Model model) {
		List<Test> testList = testService.getTest();
		model.addAttribute("testList", testList);
		return "test";
	}
	
	@ResponseBody
	@GetMapping("/test/echo")
	public String getEcho(Model model) {
		return "echo";
	}
	
}
