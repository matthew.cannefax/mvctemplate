package com.oreillyauto.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.oreillyauto.dao.TestRepository;
import com.oreillyauto.domain.Test;
import com.oreillyauto.service.TestService;

@Service("exampleService")
public class TestServiceImpl implements TestService {
    
    @Autowired
    TestRepository testRepository;

	@Override
	public List<Test> getTest() {
		return testRepository.getTest();
	}
}
