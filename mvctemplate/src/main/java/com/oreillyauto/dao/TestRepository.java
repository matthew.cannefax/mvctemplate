package com.oreillyauto.dao;

import org.springframework.data.repository.CrudRepository;

import com.oreillyauto.dao.custom.TestRepositoryCustom;
import com.oreillyauto.domain.Test;

/*
 *  CrudRepository<Example, Integer>  <== IMPORTANT: The integer is the datatype of your GUID (PK) on your table  
 */ 
public interface TestRepository extends CrudRepository<Test, Integer>, TestRepositoryCustom {
    
    // Add your interface methods here (that ARE Spring Data methods)
    
}
