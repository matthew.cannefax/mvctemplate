package com.oreillyauto.dao.custom;

import java.util.List;

import com.oreillyauto.domain.Test;

public interface TestRepositoryCustom {    
    // Add your interface methods here (that ARE NOT CRUD or Spring Data methods [concrete implementations])
    public List<Test> getTest();
}
